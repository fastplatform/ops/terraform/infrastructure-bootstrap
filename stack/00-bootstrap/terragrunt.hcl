include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::ssh://git@gitlab.com/fastplatform/ops/terraform/modules/fe-bootstrap.git"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=${get_parent_terragrunt_dir()}/terraform.tfvars"
    ]
  }
}

inputs = {
custom_policy_json = { "Version": "1.1", "Statement": [ { "Action": [ "kms:cmk:*", "kms:dek:*", "kms:grant:*", "kms:tag:*" ], "Effect": "Allow" } ] }
}