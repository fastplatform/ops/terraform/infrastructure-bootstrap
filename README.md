# Infrastructure-bootstrap

This repository is a template containing all the materials to bootstrap the infrastructure of the FaST Platform on Flexible Engine (Orange Business Services). It includes only resources that should never be changed to avoid disruption of FaST services (e.g. public IP).

[Terraform](https://github.com/hashicorp/terraform), [Terragrunt](https://github.com/gruntwork-io/terragrunt) and [Atlantis](https://www.runatlantis.io/) are used to manage the code of the infrastructure. The Terraform state is stored in a S3 Bucket on Flexible Engine. Detailed information is provided in this [document](https://gitlab.com/fastplatform/docs/-/blob/master/reference/ci-cd.md#continuous-deployment-of-the-infrastructure-using-terraform-terragrunt-and-atlantis).

This repository is structured as follows:

```bash
infrastructure-bootstrap/
  ├── stack # Terraform modules
  |     └── 00-bootstrap # Terraform module which creates bootstrap resources on Flexible Engine
  ├── terraform.tfvars.template # Configuration of the variables of the Terraform modules used
  └── terragrunt.hcl.template # Terragrunt configs
```

> The subgroup [modules](https://gitlab.com/fastplatform/ops/terraform/modules) contains the implementation of all the Terraform modules used.
